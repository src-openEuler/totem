%global gst_plugins_base_version 1.6.0
%global gtk3_version 3.19.4

Name:          totem
Epoch:         1
Version:       43.0
Release:       2
Summary:       Movie player for GNOME
License:       GPLv2+
URL:           https://wiki.gnome.org/Apps/Videos
Source0:       https://download.gnome.org/sources/%{name}/43/%{name}-%{version}.tar.xz

BuildRequires: pkgconfig(cairo)
BuildRequires: pkgconfig(gnome-desktop-3.0)
BuildRequires: pkgconfig(gsettings-desktop-schemas)
BuildRequires: pkgconfig(gstreamer-1.0)
BuildRequires: pkgconfig(gstreamer-base-1.0)
BuildRequires: pkgconfig(gstreamer-plugins-base-1.0) >= %{gst_plugins_base_version}
BuildRequires: pkgconfig(gstreamer-audio-1.0)
BuildRequires: pkgconfig(gstreamer-tag-1.0)
BuildRequires: pkgconfig(gstreamer-video-1.0)
BuildRequires: pkgconfig(gtk+-3.0) >= %{gtk3_version}
BuildRequires: pkgconfig(libportal-gtk3)
BuildRequires: pkgconfig(libhandy-1)
BuildRequires: pkgconfig(libpeas-gtk-1.0)
BuildRequires: pkgconfig(totem-plparser)
BuildRequires: pkgconfig(x11) >= 1.8
BuildRequires: gstreamer1-plugins-good
BuildRequires: gstreamer1-plugins-good-gtk
BuildRequires: gcc-c++
BuildRequires: gettext
BuildRequires: gtk-doc
BuildRequires: itstool
BuildRequires: meson
BuildRequires: python3-devel
BuildRequires: pkgconfig(pygobject-3.0)
BuildRequires: vala
BuildRequires: libappstream-glib desktop-file-utils
BuildRequires: liberation-sans-fonts
BuildRequires: pkgconfig(grilo-0.3)
BuildRequires: pkgconfig(grilo-pls-0.3)

Requires:      libpeas-loader-python3%{?_isa}
Requires:      python3-gobject
Requires:      iso-codes
Requires:      gstreamer1%{?_isa}
Requires:      gstreamer1-plugins-base%{?_isa} >= %{gst_plugins_base_version}
Requires:      gstreamer1-plugins-good%{?_isa}
Requires:      gstreamer1-plugins-good-gtk%{?_isa}
Requires:      gvfs-fuse3
Requires:      grilo-plugins%{?_isa}
Requires:      gsettings-desktop-schemas%{?_isa}
Requires:      gtk3%{?_isa} >= %{gtk3_version}

Recommends:    gstreamer1-plugin-openh264%{?_isa}
Recommends:    gstreamer1-plugins-bad-free%{?_isa}
Conflicts:     totem < 1:3.38.0-4
Obsoletes:     totem-nautilus < 1:3.31.91
Obsoletes:     totem-lirc < 1:3.33.0

Provides:      totem-nautilus = %{epoch}:%{version}-%{release}

%description
Totem is a simple movie player for the GNOME desktop. it has Simple playlists, full screen mode,
search and volume controls, and fairly complete keyboard navigation.

%package devel
Summary:             Documentation for totem
Requires:            %{name} = %{epoch}:%{version}-%{release}
%description devel
The totem-devel package contains API documentation for developing plugins for totem.

%package_help

%prep
%autosetup -p1

%build
%meson -Denable-gtk-doc=true
%meson_build -j1

%install
%meson_install
%find_lang %{name} --with-gnome

%py_byte_compile %{__python3} %{buildroot}%{_libdir}/totem/plugins/

%check
appstream-util validate-relax --nonet $RPM_BUILD_ROOT%{_datadir}/metainfo/org.gnome.Totem.appdata.xml
desktop-file-validate $RPM_BUILD_ROOT%{_datadir}/applications/org.gnome.Totem.desktop

%post
/sbin/ldconfig

%postun
/sbin/ldconfig

%files -f %{name}.lang
%doc AUTHORS NEWS README
%license COPYING
%{_bindir}/{totem,totem-video-thumbnailer}
%{_libdir}/libtotem.so.*
%{_libdir}/girepository-1.0/Totem-1.0.typelib
%{_datadir}/applications/org.gnome.Totem.desktop
%{_datadir}/dbus-1/services/org.gnome.Totem.service
%{_datadir}/metainfo/org.gnome.Totem.appdata.xml
%{_libdir}/totem/plugins/*
%{_libexecdir}/totem-gallery-thumbnailer
%{_datadir}/icons/hicolor/*/apps/*.svg
%{_datadir}/GConf/gsettings/*.convert
%{_datadir}/glib-2.0/schemas/*.xml
%{_datadir}/thumbnailers/totem.thumbnailer

%files devel
%{_includedir}/totem
%{_libdir}/libtotem.so
%{_libdir}/pkgconfig/totem.pc
%{_datadir}/gir-1.0/Totem-1.0.gir

%files help
%{_datadir}/gtk-doc/html/totem
%{_mandir}/man1/%{name}.1*
%{_mandir}/man1/totem-video-thumbnailer.1*

%changelog
* Tue Feb 25 2025 yaoxin <1024769339@qq.com> - 1:43.0-2
- Remove unused requires gstreamer1-libav

* Mon Jan 02 2023 lin zhang <lin.zhang@turbolinux.com.cn>  - 1:43.0-1
- Update to 43.0

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 1:42.0-1
- Update to 42.0 and Add totem.yaml

* Tue Jun 22 2021 weijin deng <weijin.deng@turbolinux.com.cn> - 3.38.0-1
- Upgrade to 3.38.0
- Sub package totem-nautilus becomes empty
  in version 3.31.91 Move nautilus properties page to nautilus module

* Wed Oct 28 2020 Anan Fu <fuanan3@huawei.com> - 3.30.0-1
- package init
